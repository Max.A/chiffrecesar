#Programme réalisé par ANCHER Maxime et BOULET Clément,
#élèves du département informatique, 2ème année, Groupe5.

#========================================================#

#Ici nous avons le code de la fonction de décodage circulaire,
#elle prend pour paramètres "chaine_saisie" et "decalage"
#elle retourne une chaine de caractère

def decoder_circulairement (chaine_saisie,decalage):
    string = ''
    for char in chaine_saisie:
        if ((ord(char) != 32) & (ord(char) != 10)):     #if ( (ord(char)+decalage > 122) | (ord(char)+decalage > 90) ):  #| (ord(char)> 90)
            if ( ((ord(char) >= 97) & (ord(char) <= 122)) & (ord(char)-decalage < 97) | ((ord(char) >= 65) & (ord(char) <= 90)) & (ord(char)-decalage < 65) ):        
                nouveauChar = chr(ord(char)-decalage+26)
                string = ''.join([string,nouveauChar])
            else:
                nouveauChar = chr(ord(char)-decalage)
                string = ''.join([string,nouveauChar])
        else: 
            nouveauChar = chr(ord(char))
            string= ''.join([string,nouveauChar])      
    return string
        
#Dans notre fonction nous pouvons voir "32", "10", "65", "90", "97" et "122" :
# "32" correspond au code ascii de l'espace
# "10" correspond au code ascii du retour à la ligne (backspace)
# "65" correspond au code ascii du 'A'
# "90" correspond au code ascii du 'Z'
# "97" correspond au code ascii du 'a'
# "122" correspond au code ascii du 'z'