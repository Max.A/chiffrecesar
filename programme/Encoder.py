#Programme réalisé par ANCHER Maxime et BOULET Clément,
#élèves du département informatique, 2ème année, Groupe5.

#========================================================#

#Ici nous avons le code de la fonction d'encodage non circulaire,
#elle prend pour paramètres "chaine_saisie" et "decalage"
#elle retourne une chaine de caractère

def encoder (chaine_saisie,decalage):
    string = ''
    for char in chaine_saisie:
        if ((ord(char) != 32) & (ord(char) != 10)):
            nouveauChar = chr(ord(char)+decalage)
            string = ''.join([string,nouveauChar])
        else: 
            nouveauChar = chr(ord(char))
            string= ''.join([string,nouveauChar])
    
    return string

#Dans notre fonction nous pouvons voir "32" et "10" :
# "32" correspond au code ascii de l'espace
# "10" correspond au code ascii du retour à la ligne (backspace)
