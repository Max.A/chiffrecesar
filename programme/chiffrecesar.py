#Programme réalisé par ANCHER Maxime et BOULET Clément,
#élèves du département informatique, 2ème année, Groupe5.

#========================================================#

#Différents imports dont nous aurons besoin
from tkinter import *
from Encoder import encoder
from Decoder import decoder
from EncoderCirculairement import encoder_circulairement
from DecoderCirculairement import decoder_circulairement

frame = Tk()
frame.title("Chiffre césar")

stringRetour ='Le retour de votre text se fera ici' #Cette variable s'affiche sur la zone de texte au lancement du programme

textareaUn = Text(frame, wrap='word') #Création du composant
textareaUn.insert(END, "Entrez votre text ici ") #Insertion d'un texte
chaine_saisie = textareaUn.get("0.0", END) #récupération de l'ensemble du texte dans chaine_saisie
textareaUn.grid(row=1,column=1) #Placement de la zone de text en Ligne 1, Colonne 1

textareaDeux = Text(frame, wrap='word') 
textareaDeux.insert(END, stringRetour) 
textareaDeux.get("0.0", END) 
textareaDeux.grid(row=1,column=2)

def clicEncoder(): #Définition de la fonction appelée lors du clic sur le bouton Encoder
    chaine_saisie = textareaUn.get("0.0", END).strip() 
    textareaDeux.delete("0.0", END)
    textareaDeux.insert(END, encoder(chaine_saisie,decalage.get())) #insertion du résultat de la fonction encoder

def clicEncoderCirculairement():
    chaine_saisie = textareaUn.get("0.0", END).strip() 
    textareaDeux.delete("0.0", END)
    textareaDeux.insert(END, encoder_circulairement(chaine_saisie,decalage.get())) 

def clicDecoder():
    chaine_saisie = textareaUn.get("0.0", END).strip()
    textareaDeux.delete("0.0", END)
    textareaDeux.insert(END, decoder(chaine_saisie,decalage.get()))

def clicDecoderCirculairement():
    chaine_saisie = textareaUn.get("0.0", END).strip()
    textareaDeux.delete("0.0", END)
    textareaDeux.insert(END, decoder_circulairement(chaine_saisie,decalage.get())) 

#Ici nous allons créer les différents boutons dont nous aurons besoin
boutonEncoder = Button(frame, text = "Encoder", command = lambda:clicEncoder())
boutonEncoder.grid(row=2,column=1)

boutonDecoder = Button(frame, text = "Decoder", command = lambda:clicDecoder())
boutonDecoder.grid(row=2,column=2)

boutonEncoderCirculairement = Button(frame, text = "Encoder circulairement", command = lambda:clicEncoderCirculairement())
boutonEncoderCirculairement.grid(row=3,column=1)

boutonDecoderCirculairement = Button(frame, text = "Decoder circulairement", command = lambda:clicDecoderCirculairement())
boutonDecoderCirculairement.grid(row=3,column=2)

#Ici nous allons créer le label "Décalage :" et la zone d'entrée qui récuperera le décalage
decalage = IntVar()
decalageLabel = Label(frame, text="Décalage :")
decalageLabel.grid(column = 1, row = 4)
SaisieDecalage = Entry(frame, textvariable = decalage)
SaisieDecalage.grid(column = 2, row = 4)

frame.mainloop()    