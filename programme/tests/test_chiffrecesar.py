#Programme réalisé par ANCHER Maxime et BOULET Clément,
#élèves du département informatique, 2ème année, Groupe5.

#========================================================#

#Différents imports dont nous aurons besoin
import unittest
from ChiffreCesar import encoder
from ChiffreCesar import decoder
from ChiffreCesar import encoder_circulairement
from ChiffreCesar import decoder_circulairement


class TestChiffreCesarFuctions(unittest.TestCase):
    def test_encoder(self):
        self.assertEqual(encoder('a',3),'d')
        self.assertEqual(encoder('abc',2),'cde')
        self.assertEqual(encoder(' ',3),' ')

    def test_decoder(self):
        self.assertEqual(decoder('b',1),'a')
        self.assertEqual(decoder('bcd',1),'abc')
        self.assertEqual(decoder(' ',5),' ')

    def test_encoder_circulairement(self):
        self.assertEqual(encoder_circulairement('z',1),'a')
        self.assertEqual(encoder_circulairement('zabc',2),'bcde')   
        self.assertEqual(encoder_circulairement(' ',0),' ')

    def test_decoder_circulairement(self):
        self.assertEqual(decoder_circulairement('a',1),'z')
        self.assertEqual(decoder_circulairement('bcde',2),'zabc')
        self.assertEqual(decoder_circulairement(' ',5),' ')




